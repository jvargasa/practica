package com.practice.datareader.entities;

public class Attribute {

	private String name;
	private String type;
	private boolean foreignKey;
	private boolean primaryKey;
	
	public Attribute(String name, String type) {
		super();
		this.name = name;
		this.type = type;
		this.foreignKey = false;
		this.primaryKey = false;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public boolean isForeignKey() {
		return foreignKey;
	}
	public void setForeignKey(boolean foreignKey) {
		this.foreignKey = foreignKey;
	}
	public boolean isPrimaryKey() {
		return primaryKey;
	}
	public void setPrimaryKey(boolean primaryKey) {
		this.primaryKey = primaryKey;
	}
}
