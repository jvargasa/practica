package com.practice.datareader.controllers;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.practice.datareader.entities.Greeting;

import com.practice.datareader.entities.*;

@Controller
@RequestMapping("/hello-world")
public class HelloWorldController {
	
	@Autowired
	private JdbcTemplate jdbcTemplateObject;
	
	private Connection conn;
	
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping(method=RequestMethod.GET)
    public @ResponseBody Greeting sayHello(@RequestParam(value="name", required=false, defaultValue="Stranger") String name) throws SQLException {
    	DataSource dataSource = jdbcTemplateObject.getDataSource();
    	conn =  dataSource.getConnection(); 

    	try {
    		DatabaseMetaData ds = conn.getMetaData();
    		ResultSet rs = ds.getTables(null, null, "%", null);
    		
    		DataBase dataBase = new DataBase(ds.getDatabaseProductName());
    		List<Table> tables = new ArrayList<Table>();
    		dataBase.setTables(tables);
    		while (rs.next()) {
    		 Table table = new Table();	
    		 String tableName = rs.getString(3);
    		 ResultSet columns = ds.getColumns(null, null, tableName, "%");
    		 table.setName(tableName);
    		 
    		 System.out.println(tableName);
    		 ResultSet rsp =ds.getPrimaryKeys(null, null, tableName);
    		 
    		 
//    		 ResultSet rsf =ds.getExportedKeys(null, null, tableName);
//    		 
//    		 System.out.println("____FK KEY'S_________");
//    		 while(rsf.next()){
//    			 System.out.println(rsf.getString(7));
//    		 }
    		 System.out.println("____________________");
    		 
    		 List<Attribute> attributes = new ArrayList<Attribute>();
    		 while (columns.next()) {
    			 Attribute attribute = new Attribute(columns.getString(4),columns.getString(6));
    			 attributes.add(attribute);
    			 System.out.println(columns.getString(6) +" - "+ columns.getString(4));
    		 }
    		 
    		 System.out.println("____PRIMARY KEY_____");
    		 
    		 while(rsp.next()){
    			 String primary = rsp.getString(4);
    			 
    			 for(Attribute attri : attributes){
    				 if(attri.getName().equals(primary)){
    					 attri.setPrimaryKey(true);
    				 }
    			 }
    		 }
    		 table.setAttributes(attributes);
    		 tables.add(table);
    		 System.out.println("*************************************");
    		}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }

}
